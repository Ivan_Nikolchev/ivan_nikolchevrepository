package test.cases.Jira;

import com.telerikacademy.testframework.UserActions;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public class BaseTest {

    UserActions actions = new UserActions();

    @BeforeClass
    public static void setUp() {
        UserActions.loadBrowser("jira.homepage");
    }

    @AfterClass
    public static void tearDown() {
        UserActions.quitDriver();
    }
}