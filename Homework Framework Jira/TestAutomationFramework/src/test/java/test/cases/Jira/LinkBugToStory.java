package test.cases.Jira;

import org.junit.Test;
import pages.jira.JiraHomePage;
import pages.jira.JiraLoginPage;

public class LinkBugToStory extends BaseTest {
    @Test
    public void linkIssues(){
        JiraLoginPage loginPage = new JiraLoginPage(actions.getDriver());
        loginPage.navigateToPage();
        loginPage.loginUser();

        JiraHomePage homePage = new JiraHomePage(actions.getDriver());
        homePage.linkTwoIssues();

    }
}
