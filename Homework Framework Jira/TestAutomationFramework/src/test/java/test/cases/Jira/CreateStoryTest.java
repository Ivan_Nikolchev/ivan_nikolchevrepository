package test.cases.Jira;


import org.junit.Test;
import pages.jira.JiraHomePage;
import pages.jira.JiraLoginPage;

public class CreateStoryTest extends BaseTest{

    @Test
    public void createNewStory(){
        JiraLoginPage loginPage = new JiraLoginPage(actions.getDriver());
        loginPage.navigateToPage();
        loginPage.loginUser();

        JiraHomePage homePage = new JiraHomePage(actions.getDriver());
        homePage.createJiraStory();
        homePage.validateStoryWasCreated();

    }



}

