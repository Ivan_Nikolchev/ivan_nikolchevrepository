package test.cases.Jira;

import org.junit.Test;
import pages.jira.JiraHomePage;
import pages.jira.JiraLoginPage;

import static pages.jira.Constants.BUG_SUMMARY;

public class CreateBugTest extends BaseTest{

    @Test
    public void createNewBug(){
        JiraLoginPage loginPage = new JiraLoginPage(actions.getDriver());
        loginPage.navigateToPage();
        loginPage.loginUser();

        JiraHomePage homePage = new JiraHomePage(actions.getDriver());
        homePage.createJiraBug();
        homePage.validateBugWasCreated();


    }
}

