package pages.jira;
import net.bytebuddy.asm.Advice;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import static org.junit.Assert.assertTrue;
import static pages.jira.Constants.*;


public class JiraHomePage extends BaseJiraPage {

    public JiraHomePage(WebDriver driver) {
        super(driver, "jira.homepage");
    }

    public void createJiraStory() {
        actions.waitFor(10000);
        actions.waitForElementVisible("jira.homePage.createButton");
        actions.clickElement("jira.homePage.createButton");
        actions.waitFor(2000);
        actions.waitForElementPresent("jira.homePage.issueTypeButton");
        actions.clickElement("jira.homePage.issueTypeButton");
        actions.clickElement("jira.homePage.issueTypeStory");
        actions.waitFor(5000);

//        add summary and description
        actions.typeValueInField(STORY_SUMMARY, "jira.homePage.summaryField");
        actions.waitForElementVisible("jira.homePage.descriptionField");
        actions.typeValueInField(STORY_DESCRIPTION, "jira.homePage.descriptionField");
        actions.clickElement("jira.homePage.submitIssueButton");
    }

    public void createJiraBug() {
        actions.waitFor(10000);
        actions.waitForElementVisible("jira.homePage.createButton");
        actions.clickElement("jira.homePage.createButton");
        actions.waitFor(2000);
        actions.waitForElementPresent("jira.homePage.issueTypeButton");
        actions.clickElement("jira.homePage.issueTypeButton");
        actions.clickElement("jira.homePage.issueTypeBug");
        actions.waitFor(5000);

//       add summary and description
        actions.typeValueInField(BUG_SUMMARY, "jira.homePage.summaryField");
        actions.waitForElementVisible("jira.homePage.descriptionField");
        actions.typeValueInField(BUG_DESCRIPTION, "jira.homePage.descriptionField");
        actions.clickElement("jira.homePage.submitIssueButton");
    }

    public void validateStoryWasCreated() {
        actions.waitFor(2000);
        actions.getDriver().navigate().refresh();
        actions.waitForElementPresent("jira.homePage.backlogButton");
        actions.clickElement("jira.homePage.backlogButton");
        actions.waitFor(2000);
        actions.waitForElementPresent("jira.homePage.backlogFirstResult");

        String actualString = driver.findElement(By.xpath("//div[@class='wl0viz-0 hWEUMU']")).getText();
        assertTrue("Incorrect Story Summary",actualString.contains("As a user of www.phptravels.net"));
    }

    public void validateBugWasCreated() {
        actions.waitFor(2000);
        actions.getDriver().navigate().refresh();
        actions.waitForElementPresent("jira.homePage.backlogButton");
        actions.clickElement("jira.homePage.backlogButton");
        actions.waitFor(2000);
        actions.waitForElementPresent("jira.homePage.backlogFirstResult");

        String actualString = driver.findElement(By.xpath("//div[@class='wl0viz-0 hWEUMU']")).getText();
        assertTrue("Incorrect Bug Summary",actualString.contains("The content of the link 'How to Book' is missing."));
    }

    public void linkTwoIssues(){
        actions.waitFor(10000);
        actions.waitForElementPresent("jira.homePage.backlogButton");
        actions.clickElement("jira.homePage.backlogButton");
        actions.waitFor(2000);
        driver.navigate().to(STORY_URL);
        actions.waitFor(5000);
        actions.waitForElementPresent("jira.homePage.linkIssueButton");
        actions.clickElement("jira.homePage.linkIssueButton");
        actions.waitFor(2000);
        actions.waitForElementPresent("jira.homePage.linkButton");
        actions.clickElement("jira.homePage.linkButton");
        actions.waitFor(5000);
        actions.waitForElementPresent("jira.homePage.searchForIssues");
        actions.clickElement("jira.homePage.searchForIssues");
        actions.waitFor(2000);
        Actions act = new Actions(driver);
        act.sendKeys("SHW-40");
        actions.waitFor(2000);
        act.sendKeys(Keys.ENTER);
        act.perform();
        actions.waitFor(5000);
        actions.waitForElementPresent("jira.homePage.finalLinkButton");
        actions.clickElement("jira.homePage.finalLinkButton");

    }
}

