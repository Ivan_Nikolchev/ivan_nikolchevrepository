package pages.jira;

public class Constants {
    public static final String STORY_DESCRIPTION = "As a user of www.phptravels.net, I want to get some info about the " +
            "booking procedures.\\n\\nSteps to Reproduce: \\n\\nGo to www.phptravels.net \\n\\nOn the main page choose " +
            "the link “How to Book” from the footer.";

    public static final String STORY_SUMMARY = "As a user of www.phptravels.net, I want to get some info about the " +
            "booking procedures";

    public static final String BUG_DESCRIPTION = "As a user of www.phptravels.net, I want to get some info about the " +
            "booking procedures, but the “How to Book” page is empty.\\n\\nSteps to Reproduce: \\n\\nGo to " +
            "www.phptravels.net \\n\\nOn the main page choose the link “How to Book” from the footer.\\n\\nExpected " +
            "result:\\n\\nI should get the info about the booking procedures\\n\\nActual result:\\n\\nThe “How to Book”" +
            " page is empty.\\n\\nSeverity Level: CRITICAL ";

    public static final String BUG_SUMMARY = "The content of the link 'How to Book' is missing.";
    public static final String STORY_URL = "https://ivancaterpillarbuddygroup.atlassian.net/jira/software/projects/SHW/boards/7/backlog?selectedIssue=SHW-39";

//    public static final String HOME_PAGE_ISSUE_TYPE_BUTTON = "jira.homePage.issueTypeButton";
//    public static final String HOME_PAGE_CREATE_BUTTON = "jira.homePage.createButton";

}


