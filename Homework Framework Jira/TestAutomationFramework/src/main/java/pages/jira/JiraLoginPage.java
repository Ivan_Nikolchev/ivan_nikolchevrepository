package pages.jira;
import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class JiraLoginPage extends BasePage {


    public JiraLoginPage(WebDriver driver){
        super(driver,"jira.homepage" );
    }

    public void loginUser() {

        navigateToPage();
        actions.waitForElementVisible("jira.loginPage.userName");
        actions.typeValueInField("ivan.nikolchev.a38@learn.telerikacademy.com", "jira.loginPage.userName");
        actions.clickElement("jira.loginPage.continueButton");

        actions.waitFor(2000);

        actions.waitForElementVisible("jira.loginPage.password");
        actions.typeValueInField("123qweasdzxc", "jira.loginPage.password");
        actions.clickElement("jira.loginPage.continueButton");
    }
}
