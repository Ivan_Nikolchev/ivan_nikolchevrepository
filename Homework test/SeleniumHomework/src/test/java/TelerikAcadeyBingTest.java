import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TelerikAcadeyBingTest {
    @Test
    public void navigateTelerikAcademyByBing(){
        System.setProperty("webdriver.chrome.driver","D:\\Java Projects\\ChromeDrivers\\chromedriver.exe");

        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        webDriver.get("https://www.bing.com/search?q=telerik+academy+alpha&qs=CT&pq=telerik+academy+a&sc=7-17&cvid=0CCBAD21AF934A40B6832D8CF32D83FC&FORM=QBRE&sp=1");
        webDriver.findElement(By.id("bnp_btn_accept")).click();
        String inputSearch = webDriver.findElement(By.xpath("//*[@id=\"b_results\"]/li[1]/div[1]/h2/a")).getText();


        Assert.assertEquals("IT Career Start in 6 Months - Telerik Academy Alpha",inputSearch);
        String expectedString = "IT Career Start in 6 Months - Telerik Academy Alpha";
        if(inputSearch.equalsIgnoreCase(expectedString)) {
            System.out.printf("The title '%s' is the first search result by 'Google'", inputSearch);
        }else
            System.out.println("Title didn't match");
        webDriver.quit();
    }

}
